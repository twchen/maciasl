# simulates Windows OS

# rename _OSI to XOSI
# route all _OSI calls to XOSI
into_all all code_regex _OSI replaceall_matched begin XOSI end;

# Add the XOSI method
into method label XOSI remove_entry;
into definitionblock code_regex . insert begin
Method(XOSI, 1)\n
{\n
	// the above code simulates Windows 8, because it returns true for all windows version before and including "Windows 2012"\n
	// if you want to simulate other Windows version, e.g. Windows 7, commment out "Windows 2012"\n
	// it is recommended to simulate newer version of Windows\n
	// but newer versions are not always better.\n
    // simulation targets\n
    // source: (google 'Microsoft Windows _OSI')\n
    //  http://download.microsoft.com/download/7/E/7/7E7662CF-CBEA-470B-A97E-CE7CE0D98DC2/WinACPI_OSI.docx\n
    Store(Package()\n
    {\n
        "Windows",              // generic Windows query\n
        "Windows 2001",         // Windows XP\n
        "Windows 2001 SP2",     // Windows XP SP2\n
        //"Windows 2001.1",     // Windows Server 2003\n
        //"Windows 2001.1 SP1", // Windows Server 2003 SP1\n
        "Windows 2006",         // Windows Vista\n
        "Windows 2006 SP1",     // Windows Vista SP1\n
        //"Windows 2006.1",     // Windows Server 2008\n
        "Windows 2009",         // Windows 7/Windows Server 2008 R2\n
        "Windows 2012",         // Windows 8/Windows Server 2012\n
        //"Windows 2013",       // Windows 8.1/Windows Server 2012 R2\n
        //"Windows 2015",       // Windows 10/Windows Server TP\n
    }, Local0)\n
    Return (Ones != Match(Local0, MEQ, Arg0, MTR, 0, 0))\n
}\n
end;
