# Fix the LPC Device to load AppleLPC.kext
into method label _DSM parent_label LPCB remove_entry;
into device label LPCB insert begin
Method (_DSM, 4, NotSerialized)\n
{\n
    Store (Package ()\n
        {\n
            //"built-in", Buffer (One) { 0x00 },\n
            "compatible", Buffer () { "pci8086,8c4b" },\n
            "device-id", Buffer (0x04) { 0x4B, 0x8C, 0x00, 0x00 },\n
            "IOName", Buffer (0x0D) { "pci8086,8c4b" },\n
            "name", Buffer (0x0D) { "pci8086,8c4b" }\n
        }, Local0)\n
    DTGP (Arg0, Arg1, Arg2, Arg3, RefOf (Local0))\n
    Return (Local0)\n
}\n
end;
