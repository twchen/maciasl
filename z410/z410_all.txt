### brightness keyboard shortcut patch
into method label _Q11 insert begin
Notify(\_SB.PCI0.LPCB.PS2K, 0x0205)\n
Notify(\_SB.PCI0.LPCB.PS2K, 0x0285)\n
end;

into method label _Q12 insert begin
Notify(\_SB.PCI0.LPCB.PS2K, 0x0206)\n
Notify(\_SB.PCI0.LPCB.PS2K, 0x0286)\n
end;


### battery status fix
# utility methods to read/write buffers from/to EC
into method label RE1B parent_label EC0 remove_entry;
into method label RECB parent_label EC0 remove_entry;
into device label EC0 insert
begin
Method (RE1B, 1, NotSerialized)\n
{\n
    OperationRegion(ERAM, EmbeddedControl, Arg0, 1)\n
    Field(ERAM, ByteAcc, NoLock, Preserve) { BYTE, 8 }\n
    Return(BYTE)\n
}\n
Method (RECB, 2, Serialized)\n
// Arg0 - offset in bytes from zero-based EC\n
// Arg1 - size of buffer in bits\n
{\n
    ShiftRight(Arg1, 3, Arg1)\n
    Name(TEMP, Buffer(Arg1) { })\n
    Add(Arg0, Arg1, Arg1)\n
    Store(0, Local0)\n
    While (LLess(Arg0, Arg1))\n
    {\n
        Store(RE1B(Arg0), Index(TEMP, Local0))\n
        Increment(Arg0)\n
        Increment(Local0)\n
    }\n
    Return(TEMP)\n
}\n
end;

# write to EC
into method label WE1B parent_label EC0 remove_entry;
into method label WECB parent_label EC0 remove_entry;
into device label EC0 insert
begin
Method (WE1B, 2, NotSerialized)\n
{\n
    OperationRegion(ERAM, EmbeddedControl, Arg0, 1)\n
    Field(ERAM, ByteAcc, NoLock, Preserve) { BYTE, 8 }\n
    Store(Arg1, BYTE)\n
}\n
Method (WECB, 3, Serialized)\n
// Arg0 - offset in bytes from zero-based EC\n
// Arg1 - size of buffer in bits\n
// Arg2 - value to write\n
{\n
    ShiftRight(Arg1, 3, Arg1)\n
    Name(TEMP, Buffer(Arg1) { })\n
    Store(Arg2, TEMP)\n
    Add(Arg0, Arg1, Arg1)\n
    Store(0, Local0)\n
    While (LLess(Arg0, Arg1))\n
    {\n
        WE1B(Arg0, DerefOf(Index(TEMP, Local0)))\n
        Increment(Arg0)\n
        Increment(Local0)\n
    }\n
}\n
end;

into method label MHPF code_regex \(SMD0, replaceall_matched begin (RECB(0x64, 256), end;
into method label MHIF code_regex \(FWBT, replaceall_matched begin (RECB(0x14, 64), end;
into method label GBID code_regex \(FWBT, replaceall_matched begin (RECB(0x14, 64), end;
into method label CFUN code_regex \(SMD0, replaceall_matched begin (RECB(0x64, 256), end;

into method label MHPF code_regex Store\s+\((.*),\s+SMD0\) replaceall_matched begin WECB(0x64, 256, %1) end;
into method label CFUN code_regex Store\s+\((.*),\s+SMD0\) replaceall_matched begin WECB(0x64, 256, %1) end;

### RTC fix
into device name_hid PNP0B00 code_regex (IO\s\((?:\s*[^,]+,\s*(?:\/\/\s.*)?\s*\n)+\s*)(\dx\d+)(,\s*(?:\/\/\s.*)?\s*\n\s*\)) replace_matched begin %10x02%3 end;

### Shutdown fix v2
into method label _PTS code_regex ([\s\S]*) replace_matched
begin
If (LNotEqual(Arg0,5)) {\n
%1\n
}\n
end;

### _WAK fix v2
into method label _WAK code_regex ([\s\S]*) replace_matched
begin
If (LOr(LLess(Arg0,1),LGreater(Arg0,5))) { Store(3,Arg0) }\n
%1
end;

### Remove LID0._PRW to enable sleep on lid close
into device name_hid PNP0C0D code_regex Name.*_PRW.*\n.*\n.*\n.*\n.*\}\) remove_matched;
